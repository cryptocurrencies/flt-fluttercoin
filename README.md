This Dockerfile generates the latest Fluttercoin client. It is anticipated that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name fluttercoin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/flt-fluttercoin:latest
```
