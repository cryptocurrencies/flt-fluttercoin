# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev libboost-system-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/ofeefee/fluttercoin.git /opt/fluttercoin && \
	cd /opt/fluttercoin/src/leveldb && \
	chmod +x build_detect_platform && \
	make libleveldb.a libmemenv.a && \
    cd /opt/fluttercoin/src && \
    make -f makefile.linux

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r fluttercoin && useradd -r -m -g fluttercoin fluttercoin
RUN mkdir /data
RUN chown fluttercoin:fluttercoin /data
COPY --from=build /opt/fluttercoin/src/fluttercoind /usr/local/bin/
USER fluttercoin
VOLUME /data
EXPOSE 7408 7474
CMD ["/usr/local/bin/fluttercoind", "-datadir=/data", "-conf=/data/fluttercoin.conf", "-server", "-txindex", "-printtoconsole"]